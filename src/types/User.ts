export default interface User {
  name: string;
  username: string;
  settings: string[];
}
