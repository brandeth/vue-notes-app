import type Emoji from "./Emoji";

export default interface Entry {
  id: number;
  userId: number;
  body: string;
  createdAt: Date;
  emoji: Emoji | null;
}
